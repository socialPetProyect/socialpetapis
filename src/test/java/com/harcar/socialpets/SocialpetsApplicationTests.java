package com.harcar.socialpets;

import com.harcar.socialpets.model.UsuarioEntity;
import com.harcar.socialpets.repository.IUsuarioDAO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SocialpetsApplicationTests {
    @Autowired
    private IUsuarioDAO repo;
    @Autowired
    private BCryptPasswordEncoder encoder;
    @Test
    public void contextLoads() {
    }
    @Test
    public void crearUsuarioTest(){
        UsuarioEntity us = new UsuarioEntity();
        us.setId(6);
        us.setUsername("oso");
        us.setPassword(encoder.encode("1234"));
        UsuarioEntity retorno = repo.save(us);


    }

}
