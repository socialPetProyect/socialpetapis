create database petSocialNet;
use petSocialNet;
create table users(
	userId VARCHAR(36) primary key,
	userName VARCHAR(5) not null,
	userSureName VARCHAR(5) not null,
	userMail VARCHAR(5) not null,
	userBirthDay Date not null,
	usertelePhone VARCHAR(5),
	userCreationDate Date not null,
	passWord VARCHAR(5) not null
);
create table perfil(
	perfilId VARCHAR(36) primary key,
	perfilName VARCHAR(5) not null,
	perfilType VARCHAR(5) not null,
	perfilRace VARCHAR(5),
	perfilDescription VARCHAR(5),
	perfilBirthDay Date,
	perfilCreationDay Date not null,
	userId VARCHAR(36) not null,
	foreign key (userId) references users(userId)
);
create table login(
	loginUser VARCHAR(5),
	loginPass VARCHAR(5),
	userId VARCHAR(36),
	primary key(loginUser, loginPass),
	foreign key (userId) references perfil(userId)
);
create table publications(
	publicationTitle VARCHAR(5),
	publicationImg VARCHAR(5),
	publicationDescription VARCHAR(5) not null,
	publicationCreationDate Date not null,
	perfilId VARCHAR(36) not null,
	foreign key (perfilId) references perfil(perfilId)
);
create table follow(
	followId VARCHAR(36) not null,
	followFan VARCHAR(36) not null,
	followIdol VARCHAR(36) not null,
	primary key(followFan, followId)
);
/* Restricciones de campos de la tabla de perfiles */
/*
alter table perfil add
constraint perfilName_uni unique (perfilName),
*/
/* Login */
