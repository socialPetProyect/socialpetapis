package com.harcar.socialpets.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.i18n.SessionLocaleResolver;

import java.util.Locale;

/**
 * Clase que implimenta a WebMvcConfigurer , para poder complementar y modificar la configuración
 * Al anotar esta clase con @Configuration , cuando arranca el contexto de Spring lee esta clase y carga en el contexto
 * aquellos metodos que esten anotados con @Bean para poder ser inyectados en el resto de la aplicación.
 */
@Configuration
public class MvcConfig implements WebMvcConfigurer {

      //  private final Logger log = LoggerFactory.getLogger(getClass());
    /**
     * Metodo que lo utilizaremos para manejar los recursos que estan fuera del proyecto.
     * Agregamos la ruta donde estarán los recursos externos , y le vamos a indicar a spring donde está y que lo considere
     * como un directorio del proyecto , fuera del JAR.
     * @param registry
     */
//    @Override
//    public void addResourceHandlers(ResourceHandlerRegistry registry) {
//        //Creamos la ruta absoluta , lo convertimos a una Uri agrega el esquema File y despues lo hacmoes string.
//        String resourcePath = Paths.get("uploads").toAbsolutePath().toUri().toString();
//        log.info(resourcePath);
//        registry.addResourceHandler("/uploads/**")
//                .addResourceLocations(resourcePath);
//
//    }

    /**
     * Este metodo vamos a utilizarlo para generar 1 controlador de vistas , sin necesidad de generar 1 clase,
     *  Este metodo recive 1 objeto ViewControllerRegistri , que solo carga la vista pero no tiene lógica del controlador.
     *  con el setViewName , pasamos el nombre de la vista que vamos a enviar
     * @param registry
     */
    public void  addViewControllers(ViewControllerRegistry registry){
        registry.addViewController("/error_403").setViewName("error_403");

    }
    /**
     * Metodo que vamos a usar para definir nuestro idioma por defecto que se va a guardar en la sessión Http.
     */
    @Bean
    public LocaleResolver localeResolver (){
        SessionLocaleResolver localeResolver = new SessionLocaleResolver();
        localeResolver.setDefaultLocale(new Locale("es", "ES"));
        return localeResolver;
    }

    /**
     * Metodo que vamos a utilizar para codificar el password , utilizaremos BCryp, es uno de lo mas potentes para
     * spring securiry.
     * Anotamos con @Bean para que podamos inyectar este método desde la aplicación como un componente de Spring.
     * @return
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder (){
        return  new BCryptPasswordEncoder();
    }

}
